package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class BankaController {
    @FXML
    public GridPane banka;

    public void zpetButtBanka(ActionEvent actionEvent) throws IOException {
        Parent viewUvod = FXMLLoader.load(getClass().getResource("/uvodni_obrazovka.fxml"));
        Scene sceneUvod = new Scene(viewUvod);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneUvod);
        window.show();
    }

    public void PrehledUveruClick(ActionEvent actionEvent) throws IOException {
        Parent viewJednPlatba = FXMLLoader.load(getClass().getResource("/Prehled_uveru.fxml"));
        Scene sceneJednPlatba = new Scene(viewJednPlatba);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneJednPlatba);
        window.show();
    }

    public void TransakcniHistorieClick(ActionEvent actionEvent) throws IOException {
        Parent viewHistorie = FXMLLoader.load(getClass().getResource("/Transakcni_historie.fxml"));
        Scene sceneHistorie = new Scene(viewHistorie);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneHistorie);
        window.show();
    }
}
