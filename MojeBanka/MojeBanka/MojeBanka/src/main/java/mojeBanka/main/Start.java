/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package mojeBanka.main;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
//import logika.*;
//import uiText.TextoveRozhrani;

/*******************************************************************************
 * T&#x159;&iacute;da {@code Start} je hlavn&iacute; t&#x159;&iacute;dou projektu,
 * kter&yacute; ...
 *
 * @author jm&eacute;no autora
 * @version 0.00.000
 */
public class Start extends Application {
    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args) {
        if(args.length>0 && args[0].equals("-text")) {

        } else { // pokud parametr není text, spustí se v grafickém

            //spuštění grafického okna
            launch();
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/uvodni_obrazovka.fxml"));
        primaryStage.setTitle("MojeBanka");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        WebView webView = new WebView();
        webView.getEngine().load(getClass().getResource("/cas.fxml").toExternalForm());
        Stage stage = new Stage();
        stage.setTitle("Čas");
        stage.setScene(new Scene(webView, 250, 100));
        stage.setX(1300);
        stage.setY(200);
        stage.show();
    }
}
