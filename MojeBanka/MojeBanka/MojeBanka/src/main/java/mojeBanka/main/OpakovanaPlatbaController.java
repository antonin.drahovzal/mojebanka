package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class OpakovanaPlatbaController {
    public void OpakZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewZpet = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneZpet = new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }

    public void TrvalyPrikazClick(ActionEvent actionEvent) throws IOException {
        Parent viewTrvalyPrikaz = FXMLLoader.load(getClass().getResource("/Trvaly_prikaz.fxml"));
        Scene sceneTrvalyPrikaz = new Scene(viewTrvalyPrikaz);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneTrvalyPrikaz);
        window.show();
    }
}
