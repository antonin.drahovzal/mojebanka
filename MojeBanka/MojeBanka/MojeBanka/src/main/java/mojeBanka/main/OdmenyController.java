package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import mojeBanka.logika.Odmeny;

import java.io.IOException;

public class OdmenyController {
    @FXML
    public ListView odmenyId;

    private Odmeny odmeny;

    public OdmenyController()
    {
        odmeny = new Odmeny();
    }

    public void initialize()
    {
        odmenyId.getItems().addAll(odmeny.getUcty());
    }

    public void OdmenyZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewZpet = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneZpet= new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }

}
