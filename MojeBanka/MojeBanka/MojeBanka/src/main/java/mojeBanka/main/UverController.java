package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import mojeBanka.logika.Uver;

import java.io.IOException;

public class UverController {
    @FXML
    public TextField castkaId;

    @FXML
    public Label splatkaId;

    @FXML
    public Label dobaId;

    private Uver uver;

    public UverController()
    {
        //uver = new Uver();
    }


    public void UverZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewZpet = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneZpet= new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }



    public void PozadatClick(ActionEvent actionEvent) throws IOException {
        String text = castkaId.getText();
        int castka = Integer.parseInt(text);
        //uver.PricteniDluhu(castka);

        Parent viewZpet = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneZpet= new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }

    public void SpocitatClick(ActionEvent actionEvent) {
        String text = castkaId.getText();
        int castka = Integer.parseInt(text);
        uver.Pocitej(castka);

        splatkaId.setText(String.valueOf(uver.getSplatka()));
        dobaId.setText(String.valueOf(uver.getDoba()));
    }
}
