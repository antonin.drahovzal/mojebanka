package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Transakcni_historie_ucet {
    public void TransakceZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewJednPlatba = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneJednPlatba = new Scene(viewJednPlatba);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneJednPlatba);
        window.show();
    }
}
