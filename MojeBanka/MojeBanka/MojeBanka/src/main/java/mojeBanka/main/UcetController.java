package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.*;
import mojeBanka.logika.*;
import mojeBanka.main.KontokorentController;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

public class UcetController {
    @FXML
    public Label CisloUctuId;

    @FXML
    public Label ZustatekId;

    @FXML
    public Label stavId;

    private BankovniUcet vybranyUcet;

    private Kontokorent kontokorent;
    private MojeBanka mb;


    public UcetController(){
        kontokorent = new Kontokorent();
        vybranyUcet = new BankovniUcet();
        mb = new MojeBanka();
    }

    public void initialize()
    {
        //ZustatekId.setText(String.valueOf(vybranyUcet.zustatekUctuJedna()));
    }

    public void myFunction(String text){
        stavId.setText(text);
    }

    public void setData(String ucet){
        Collection<BankovniUcet> bus = mb.getUcty();
        for(BankovniUcet bu : bus){
            if(bu.getCisloUctu() == ucet){
                CisloUctuId.setText(bu.getCisloUctu());
                ZustatekId.setText(Integer.toString(bu.getZustatek()));
                if(bu.getKontokorent() == true){
                    stavId.setText("Aktivní");
                }else stavId.setText("Neaktivní");
            }
        }
    }



/*
    public void initData(BankovniUcet ucet)
    {
        vybranyUcet = ucet;
        CisloUctuId.setText(vybranyUcet.getCisloUctu());
        ZustatekId.setText(Integer.toString(vybranyUcet.getZustatek()));
    }

 */



    public void UcetZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewZpet = FXMLLoader.load(getClass().getResource("/uvodni_obrazovka.fxml"));
        Scene sceneZpet= new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }

    public void OdmenyCLick(ActionEvent actionEvent) throws IOException {
        Parent viewOdmeny = FXMLLoader.load(getClass().getResource("/Odmeny_za_platby.fxml"));
        Scene sceneOdmeny= new Scene(viewOdmeny);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneOdmeny);
        window.show();
    }

    public void KontokorentClick(ActionEvent actionEvent) throws IOException {
        Parent viewKontokorent = FXMLLoader.load(getClass().getResource("/Kontokorent.fxml"));
        Scene sceneKontokorent = new Scene(viewKontokorent);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneKontokorent);
        window.show();
    }

    public void UverClick(ActionEvent actionEvent) throws IOException {
        Parent viewUver = FXMLLoader.load(getClass().getResource("/Uvěr.fxml"));
        Scene sceneUver = new Scene(viewUver);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneUver);
        window.show();
    }

    public void OpakPlatbaClick(ActionEvent actionEvent) throws IOException {
        Parent viewOpakPlatba = FXMLLoader.load(getClass().getResource("/Opakovana_platba.fxml"));
        Scene sceneOpakPlatba = new Scene(viewOpakPlatba);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneOpakPlatba);
        window.show();
    }

    public void JednorazovaPlatbaClick(ActionEvent actionEvent) throws IOException {
        Parent viewJednPlatba = FXMLLoader.load(getClass().getResource("/Jednorazova_platba.fxml"));
        Scene sceneJednPlatba = new Scene(viewJednPlatba);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneJednPlatba);
        window.show();
    }

    public void HistorieClick(ActionEvent actionEvent) throws IOException {
        Parent viewJednPlatba = FXMLLoader.load(getClass().getResource("/Transakcni_historie_ucet.fxml"));
        Scene sceneJednPlatba = new Scene(viewJednPlatba);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneJednPlatba);
        window.show();
    }
}
