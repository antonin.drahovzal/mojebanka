package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class JednorazovaPlatbaController {
    public void JednorazovaZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewZpet = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneZpet = new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }

    public void PoslatClick(ActionEvent actionEvent) {
    }
}
