package mojeBanka.main;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mojeBanka.logika.Kontokorent;
import mojeBanka.main.UcetController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class KontokorentController implements Initializable{
    private Kontokorent kontokorent;

    public KontokorentController()
    {
        kontokorent = new Kontokorent();
    }

    public void KontokorentZpetClick(ActionEvent actionEvent) throws IOException {
        Parent viewZpet = FXMLLoader.load(getClass().getResource("/Ucet.fxml"));
        Scene sceneZpet= new Scene(viewZpet);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(sceneZpet);
        window.show();
    }

    public void PozadatClick(ActionEvent actionEvent) throws IOException {
        kontokorent.setKontokorent(true);
        FXMLLoader loader =  new FXMLLoader(getClass().getResource("/Ucet.fxml"));
        Parent root = (Parent) loader.load();
        UcetController ucetController = loader.getController();
        ucetController.myFunction("Kontokorent");

        Scene newScene = new Scene(root);
        Stage newStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        newStage.setScene(newScene);
        newStage.show();

    }

    public void ZrusitClick(ActionEvent actionEvent) throws IOException {
        kontokorent.setKontokorent(false);
        FXMLLoader loader =  new FXMLLoader(getClass().getResource("/Ucet.fxml"));
        Parent root = (Parent) loader.load();
        UcetController ucetController = loader.getController();
        ucetController.myFunction("Nic");

        Scene newScene = new Scene(root);
        Stage newStage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        newStage.setScene(newScene);
        newStage.show();
    }

    public void initialize(URL location, ResourceBundle resources) {

    }


}
