package mojeBanka.main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import mojeBanka.logika.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class UvodniObrazovkaController {
    @FXML
    public Button bankaButton;
    @FXML
    public ListView ucty;


    private MojeBanka mojeBanka;
    private BankovniUcet bankovniUcet;
    private ArrayList<String> uctyKZobrazeni;

    public UvodniObrazovkaController()
    {
        mojeBanka = new MojeBanka();
        uctyKZobrazeni = new ArrayList<String>();
    }

    public void initialize()
    {
        for(BankovniUcet bu : mojeBanka.getUcty()){
            uctyKZobrazeni.add(bu.getCisloUctu());
        }
        ucty.getItems().addAll(uctyKZobrazeni);
    }

    public void OnUcetDetailClick(MouseEvent mouseEvent) throws IOException {
        FXMLLoader loader =  new FXMLLoader(getClass().getResource("/Ucet.fxml"));
        Parent root = (Parent) loader.load();
        UcetController ucetController = loader.getController();
        ucetController.setData((String) ucty.getSelectionModel().getSelectedItem());

        Scene newScene = new Scene(root);
        Stage newStage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        newStage.setScene(newScene);
        newStage.show();
    }
/*
    public void ZmenaDatUctu(MouseEvent mouseEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/Ucet.fxml"));
        Parent viewUcet = loader.load();
        Scene sceneUcet= new Scene(viewUcet);

        UcetController controller = loader.getController();
        controller.initData(ucty.getSelectionModel().getSelectedItem());


    }

 */
}
