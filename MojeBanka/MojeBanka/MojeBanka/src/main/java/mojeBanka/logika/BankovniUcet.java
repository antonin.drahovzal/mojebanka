package mojeBanka.logika;

import java.util.*;

public class BankovniUcet {
    private int zustatek;
    private String cisloUctu;
    private boolean kontokorent;


    public BankovniUcet()
    {

    }

    public BankovniUcet(String cislouctu, int zustatek, boolean kontokorent)
    {
       this.zustatek = zustatek;
       this.cisloUctu = cislouctu;
       this.kontokorent = kontokorent;
    }

    public int getZustatek() {return this.zustatek;}

    public void setZustatek(int zustatek){
        this.zustatek = zustatek;
    }

    public String getCisloUctu(){return this.cisloUctu;}

    public void setCisloUctu(String cislouctu){
        this.cisloUctu = cislouctu;
    }

    public boolean getKontokorent(){return this.kontokorent;}

    public void setKontokorent(boolean _kontokorent){
        this.kontokorent = _kontokorent;
    }
   // public Collection<String> getCislaUctu()
    //{
     //   return Collections.unmodifiableCollection(this.cislaUctu);
    //}





}
