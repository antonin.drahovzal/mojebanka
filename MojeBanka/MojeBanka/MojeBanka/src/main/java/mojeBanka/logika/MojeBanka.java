package mojeBanka.logika;
import java.util.*;

public class MojeBanka {
    private Set<BankovniUcet> ucty;

    public MojeBanka()
    {
        this.ucty = new HashSet<BankovniUcet>();
        BankovniUcet buJedna = new BankovniUcet("123",1500,false);
        BankovniUcet buDva = new BankovniUcet("456",2000,false);
        BankovniUcet budTri = new BankovniUcet("789",2500,true);

        ucty.add(buJedna);
        ucty.add(buDva);
        ucty.add(budTri);
    }

    public Collection<BankovniUcet> getUcty()
    {
        return this.ucty;
    }
}
