package mojeBanka.logika;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Kontokorent {
    private boolean kontokorent;

    private Set<String> kontokorenty;

    public Kontokorent()
    {
        this.kontokorenty = new HashSet();

        kontokorenty.add("Kontokorent");
    }

    public Collection<String> getKontokorenty()
    {
        return this.kontokorenty;
    }

    public Boolean getKontokorent(){
        return this.kontokorent;
    }

    public void setKontokorent(Boolean kontokorent)
    {
        this.kontokorent = kontokorent;
    }
}
